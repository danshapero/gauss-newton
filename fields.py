import firedrake
from firedrake import sin, cos, sqrt
from numpy import random, pi as π


def random_trig_poly(mesh, degree, Lx=1.0, Ly=1.0, rng=random.RandomState()):
    a = rng.normal(size=(degree, degree))
    b = rng.normal(size=(degree, degree))

    x, y = firedrake.SpatialCoordinate(mesh)

    def term(k, l):
        φ = 2 * π * (k * x/Lx + l * y/Ly)
        return (a[k, l] * sin(φ) + b[k, l] * cos(φ)) / sqrt(1 + k**2 + l**2)

    return sum((term(k, l)
                for k in range(degree)
                for l in range(degree) if k**2 + l**2 <= degree**2))

