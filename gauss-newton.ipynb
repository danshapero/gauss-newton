{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "%matplotlib inline\n",
    "import numpy as np\n",
    "from numpy import random\n",
    "import firedrake\n",
    "from firedrake import inner, grad, dx\n",
    "from fields import random_trig_poly\n",
    "\n",
    "def solve(*args, **kwargs):\n",
    "    firedrake.solve(*args, **kwargs,\n",
    "                    solver_parameters={'ksp_type': 'preonly', 'pc_type': 'lu'})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Setup\n",
    "\n",
    "First, we'll make a random resistivity field $A$ and a random forcing function $f$.\n",
    "Then we'll find the minimizer $U$ of the convex functional\n",
    "\n",
    "$$W(u) = \\int_\\Omega\\left(\\frac{1}{2}k\\nabla u\\cdot\\nabla u - fu\\right)dx.$$\n",
    "\n",
    "The exact field $U$ cannot be observed in real inverse problems, so we'll artificially add some noise.\n",
    "The noise amplitude will be 5% of the $L^2$-norm of the solution $U$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Nx, Ny = 32, 32\n",
    "mesh = firedrake.UnitSquareMesh(Nx, Ny)\n",
    "\n",
    "degree = 2\n",
    "Q = firedrake.FunctionSpace(mesh, 'CG', degree, name='parameter')\n",
    "V = firedrake.FunctionSpace(mesh, 'CG', degree, name='state')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rng = random.RandomState()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "k = firedrake.interpolate(firedrake.exp(0.1 * random_trig_poly(mesh, 4, rng=rng)), Q)\n",
    "firedrake.plot(k, cmap='magma')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = firedrake.interpolate(random_trig_poly(mesh, 3, rng=rng), V)\n",
    "firedrake.plot(f, cmap='magma')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "U = firedrake.Function(V)\n",
    "A = (0.5 * k * inner(grad(U), grad(U)) - f * U) * dx\n",
    "\n",
    "bc = firedrake.DirichletBC(V, 0, 'on_boundary')\n",
    "solve(firedrake.derivative(A, U) == 0, U, bc)\n",
    "firedrake.plot(U, cmap='magma')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "area = firedrake.assemble(firedrake.Constant(1) * dx(mesh))\n",
    "σ = 0.05 * firedrake.norm(U) / area**0.5\n",
    "\n",
    "u_obs = U.copy(deepcopy=True)\n",
    "size = u_obs.dat.data_ro.shape\n",
    "u_obs.dat.data[:] += σ * rng.normal(size=size)\n",
    "firedrake.plot(u_obs, cmap='magma')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Adjoint method\n",
    "\n",
    "Next, we'll show how to compute the derivative of the objective functional with respect to the log-conductivity using the adjoint method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "θ = firedrake.interpolate(firedrake.Constant(0), Q)\n",
    "u = firedrake.Function(V)\n",
    "A = (0.5 * firedrake.exp(θ) * inner(grad(u), grad(u)) - f * u) * dx\n",
    "F = firedrake.derivative(A, u)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "solve(F == 0, u, bc)\n",
    "firedrake.plot(u, cmap='magma')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "δu = firedrake.interpolate(U - u, V)\n",
    "firedrake.plot(δu)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "E = 0.5 * ((u - u_obs) / σ)**2 * dx\n",
    "dE = firedrake.derivative(E, u)\n",
    "\n",
    "λ = firedrake.Function(V)\n",
    "dF_du = firedrake.derivative(F, u)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "solve(firedrake.adjoint(dF_du) == -dE, λ, bc)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "firedrake.plot(λ, cmap='magma')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dF_dθ = firedrake.derivative(F, θ)\n",
    "dJ = firedrake.action(firedrake.adjoint(dF_dθ), λ)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p = firedrake.interpolate(0.05 * random_trig_poly(mesh, 3, rng=rng), Q)\n",
    "dJ_dp = firedrake.assemble(firedrake.action(dJ, p))\n",
    "print(dJ_dp)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "E_0 = firedrake.assemble(E)\n",
    "\n",
    "N = 12\n",
    "Es = np.zeros(N)\n",
    "\n",
    "for n in range(N):\n",
    "    t = 1.0/2.0**(n + 1)\n",
    "    θ_t = firedrake.interpolate(θ + firedrake.Constant(t) * p, Q)\n",
    "    u_t = firedrake.Function(Q)\n",
    "    F_t = firedrake.replace(F, {u: u_t, θ: θ_t})\n",
    "    solve(F_t == 0, u_t, bc)\n",
    "    E_t = firedrake.replace(E, {u: u_t})\n",
    "    Es[n] = firedrake.assemble(E_t)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ts = np.array([1.0/2**(n + 1) for n in range(N)])\n",
    "c = np.polyfit(np.log2(ts), np.log2(abs(Es - (E_0 + ts * dJ_dp))), 2)\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Gauss-Newton approximation\n",
    "\n",
    "Finally, we'll compute the Gauss-Newton approximation to the Hessian and show that adding this term gives us more local information about the objective functional."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ϕ = firedrake.Function(V)\n",
    "ψ = firedrake.Function(V)\n",
    "\n",
    "g = firedrake.action(dF_dθ, p)\n",
    "solve(dF_du == g, ϕ, bc)\n",
    "h = firedrake.derivative(dE, u, ϕ)\n",
    "solve(firedrake.adjoint(dF_du) == h, ψ, bc)\n",
    "Hpp = firedrake.assemble(firedrake.action(firedrake.action(firedrake.adjoint(dF_dθ), ψ), p))\n",
    "print(Hpp)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By plotting the local linear and quadratic approximations to the objective, we find that the Gauss-Newton approximation reduces the overall error by a factor of 2-4."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "fig, ax = plt.subplots()\n",
    "ax.plot(np.log2(ts), np.log2(abs(Es - (E_0 + ts * dJ_dp))), color='r')\n",
    "ax.plot(np.log2(ts), np.log2(abs(Es - (E_0 + ts * dJ_dp + ts**2/2 * Hpp))), color='b')\n",
    "plt.show(fig)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A log-log fit suggests that the error in the local approximation is definitely lower (the last coefficient).\n",
    "For the full Hessian, we would expect the second coefficient to be 3.\n",
    "Usually the fit for the Gauss-Newton Hessian suggests a second-order local approximation error, sometimes higher."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.polyfit(np.log2(ts), np.log2(abs(Es - (E_0 + ts * dJ_dp + ts**2/2 * Hpp))), 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Solution via gradient descent\n",
    "\n",
    "As a baseline, we'll try to solve the inverse problem using gradient descent."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L = 1/3\n",
    "from inverse import InverseProblem\n",
    "problem = InverseProblem(\n",
    "    θ_initial=θ,\n",
    "    f=f,\n",
    "    u_obs=u_obs,\n",
    "    σ=σ,\n",
    "    L=L\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for n in range(20):\n",
    "    problem.step()\n",
    "    E, R = problem.objective, problem.regularization\n",
    "    print(firedrake.assemble(E), firedrake.assemble(R))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "K = firedrake.interpolate(firedrake.exp(problem.parameter), Q)\n",
    "firedrake.plot(K, cmap='magma')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "firedrake.plot(k, cmap='magma')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Solving the Gauss-Newton system\n",
    "\n",
    "We get a perfectly serviceable inverse solver using gradient descent.\n",
    "By using the Gauss-Newton approximation $H$ to the true Hessian $d^2J$, we might get much faster convergence.\n",
    "To do this, we'll create an object that multiplies a search direction in parameter space by $H$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from inverse import GaussNewton\n",
    "E = 0.5 * ((u - u_obs) / σ)**2 * dx\n",
    "R = L**2/2 * inner(grad(θ), grad(θ)) * dx\n",
    "G = GaussNewton(θ, u, F, E, R)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Gauss-Newton object has a method `mult` that multplies a field by $H$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from firedrake.petsc import PETSc\n",
    "\n",
    "H = PETSc.Mat().create()\n",
    "\n",
    "with p.dat.vec_ro as p_vec:\n",
    "    size = p_vec.getSize()\n",
    "H.setSizes(size, size)\n",
    "\n",
    "H.setType(H.Type.PYTHON)\n",
    "H.setPythonContext(G)\n",
    "H.setUp()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "M = firedrake.assemble(firedrake.TrialFunction(Q) * firedrake.TestFunction(Q) * dx)\n",
    "\n",
    "ksp = PETSc.KSP().create()\n",
    "ksp.setOperators(H, M.M.handle)\n",
    "ksp.setUp()\n",
    "ksp.setType('cg')\n",
    "ksp.setTolerances(rtol=5e-3, atol=0, max_it=100)\n",
    "pc = ksp.getPC()\n",
    "pc.setType('none')\n",
    "ksp.setTolerances()\n",
    "ksp.setConvergenceHistory()\n",
    "ksp.setFromOptions()\n",
    "\n",
    "q = firedrake.Function(Q)\n",
    "z = firedrake.assemble(-dJ)\n",
    "\n",
    "with q.dat.vec as q_vec:\n",
    "    with z.dat.vec_ro as z_vec:\n",
    "        ksp.solve(z_vec, q_vec)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "firedrake.plot(q, cmap='magma')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(firedrake.assemble(firedrake.action(dJ, q)))\n",
    "print(ksp.getIterationNumber())\n",
    "print(ksp.getRhs().norm())\n",
    "print(ksp.getResidualNorm())\n",
    "ksp.getConvergenceHistory()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To use the Gauss-Newton operator, we'll sublcass the `InverseProblem` object and override the `compute_search_direction` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class GaussNewtonInverseProblem(InverseProblem):\n",
    "    def __init__(self, θ_initial, f, u_obs, σ, L):\n",
    "        self._setup(θ_initial, f, u_obs, σ, L)\n",
    "\n",
    "        self._G = GaussNewton(self._parameter, self._state, self._weak_form,\n",
    "                              self._objective, self._regularization)\n",
    "        \n",
    "        self._H = PETSc.Mat().create()\n",
    "        with self._parameter.dat.vec_ro as p_vec:\n",
    "            size = p_vec.getSize()\n",
    "            self._H.setSizes(size, size)\n",
    "        self._H.setType(self._H.Type.PYTHON)\n",
    "        self._H.setPythonContext(self._G)\n",
    "        self._H.setUp()\n",
    "\n",
    "        Q = self._parameter.function_space()\n",
    "        ϕ, ψ = firedrake.TrialFunction(Q), firedrake.TestFunction(Q)\n",
    "        self._M = firedrake.assemble(ϕ * ψ * dx)\n",
    "        \n",
    "        self._update()\n",
    "\n",
    "    def compute_search_direction(self):\n",
    "        ksp = PETSc.KSP().create()\n",
    "        ksp.setOperators(self._H, self._M.M.handle)\n",
    "        ksp.setUp()\n",
    "        ksp.setType('cg')\n",
    "        ksp.setTolerances(rtol=5e-3, atol=0, max_it=100)\n",
    "        pc = ksp.getPC()\n",
    "        pc.setType('none')\n",
    "        ksp.setTolerances()\n",
    "        ksp.setConvergenceHistory()\n",
    "        ksp.setFromOptions()\n",
    "\n",
    "        q, dJ = self.search_direction, self.derivative\n",
    "        f = firedrake.assemble(-dJ)\n",
    "\n",
    "        with q.dat.vec as q_vec:\n",
    "            with f.dat.vec_ro as f_vec:\n",
    "                ksp.solve(f_vec, q_vec)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gauss_newton_problem = GaussNewtonInverseProblem(\n",
    "    θ_initial=θ,\n",
    "    f=f,\n",
    "    u_obs=u_obs,\n",
    "    σ=σ,\n",
    "    L=L\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "q = gauss_newton_problem.search_direction\n",
    "firedrake.plot(q, cmap='magma')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "E, R, dJ = gauss_newton_problem.objective, gauss_newton_problem.regularization, gauss_newton_problem.derivative\n",
    "print(firedrake.assemble(E), firedrake.assemble(R), firedrake.assemble(firedrake.action(dJ, q)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for n in range(20):\n",
    "    gauss_newton_problem.step()\n",
    "    print(firedrake.assemble(E), firedrake.assemble(R), firedrake.assemble(firedrake.action(dJ, q)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "K = firedrake.interpolate(firedrake.exp(gauss_newton_problem.parameter), Q)\n",
    "firedrake.plot(K, cmap='magma')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "firedrake.plot(k, cmap='magma')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "θ_descent, θ_gauss_newton = problem.parameter, gauss_newton_problem.parameter\n",
    "δθ = firedrake.interpolate(θ_gauss_newton - θ_descent, Q)\n",
    "firedrake.plot(δθ)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "firedrake",
   "language": "python",
   "name": "firedrake"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
