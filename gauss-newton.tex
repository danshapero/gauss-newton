\documentclass{article}

\usepackage{amsmath}
%\usepackage{amsfonts}
\usepackage{amsthm}
%\usepackage{amssymb}
%\usepackage{mathrsfs}
%\usepackage{fullpage}
%\usepackage{mathptmx}
%\usepackage[varg]{txfonts}
\usepackage{color}
\usepackage[charter]{mathdesign}
\usepackage[pdftex]{graphicx}
%\usepackage{float}
%\usepackage{hyperref}
%\usepackage[modulo, displaymath, mathlines]{lineno}
%\usepackage{setspace}
%\usepackage[titletoc,toc,title]{appendix}

%\linenumbers
%\doublespacing

\theoremstyle{definition}
\newtheorem*{defn}{Definition}
\newtheorem*{exm}{Example}

\theoremstyle{plain}
\newtheorem*{thm}{Theorem}
\newtheorem*{lem}{Lemma}
\newtheorem*{prop}{Proposition}
\newtheorem*{cor}{Corollary}

\newcommand{\argmin}{\text{argmin}}
\newcommand{\ud}{\hspace{2pt}\mathrm{d}}
\newcommand{\bs}{\boldsymbol}
\newcommand{\PP}{\mathsf{P}}

\title{The Gauss-Newton approximation to the Hessian}
\author{Daniel Shapero}
\date{}

\begin{document}

\maketitle

\section{PDE-constrained optimization}

In the following, we'll discuss the Gauss-Newton approximation to the Hessian of nonlinear functionals that arise in PDE-constrained optimization.
In PDE-constrained optimization, we seek to minimize some functional $E$ of the solution $u$ of some partial differential equation by making some kind of smart choice for a parameter $p$ of the PDE.
The state variable $u$ lives in some function space $V$, while the parameters $p$ live in a function space $Q$.
We will write the PDE as
\begin{equation}
    F(u, p) = 0,
\end{equation}
where $F : V \times Q \to V^*$ is the weak form of the PDE and $V^*$ is the dual of $V$.
We make the assumption that $\partial F/\partial u$, viewed as a linear operator from $V$ to $V^*$, is invertible.
By the implicit function theorem, there is a solution operator $G(p)$ such that
\begin{equation}
    F(G(p), p) = 0.
\end{equation}
Finally, since these problems are often ill-posed otherwise, we add a regularization functional $R(p)$ to the objective to account for prior information or constraints on $p$.
Putting all these together, we can then define the objective functional
\begin{equation}
    J(p) = E(G(p)) + R(p)
\end{equation}
and our problem is to minimize $J$.

Using the chain rule, the implicit function theorem, and the properties of adjoint operators, we can come up with a computable formula for the directional derivative of the objective with respect to $p$ along a field $q$:
\begin{equation}
    \left\langle\frac{d}{dp}E(G(p)), q\right\rangle_Q = \left\langle\frac{\partial F}{\partial p}^*\lambda, q\right\rangle_Q
\end{equation}
where the \emph{adjoint state} $\lambda$ is the solution of the PDE
\begin{equation}
    \frac{\partial F}{\partial u}^*\lambda + dE = 0.
\end{equation}
The well-posedness assumptions on $F$ guarantee that the adjoint state exists.

Being able to compute the derivative of the objective functional is a key ingredient in designing procedures for solving PDE-constrained optimization problems, but it's not all we need.
The objective functional is a nonlinear map from $Q$ to the real numbers, so its derivative at any candidate value of $p$ is an element of the dual space $Q^*$.
For an arbitrary Banach space, the dual space is not the same as the primal.
If we want to use the derivative of the objective functional to come up with a search direction, we need a way to somehow get from the dual space $Q^*$ back to the primal space $Q$.

To make matters even more complicated, the parameter space $Q$ is often pretty weird.
Provided that the forward model physics are an elliptic PDE, the observed field $u$ is an element of the Sobolev space $H^1$, which is a Hilbert space.
Not so for, say, the diffusion coefficient of an elliptic PDE, which must be bounded and positive; in that case, one would want to look in $L^\infty$, which is not a Hilbert space.
Even if we took $Q$ to be one of the Sobolev spaces $H^s$, which are Hilbert spaces, calculating the isomorphism between the primal and the dual space involves solving a PDE.
And even if it were plain old $L^2$, the Riesz representer for this isomorphism is the Galerkin mass matrix.


\section{Newton's method}

The Right Thing to Do is to use Newton's method.
The second derivative operator $d^2J$ is a linear map from $Q$ to $Q^*$, so if we choose a search direction $q$ to be the solution of the linear system
\begin{equation}
    d^2J(p)\cdot q = -dJ(p)
\end{equation}
then the result is just what we need: it lives in $Q$ and not its dual.

We then face the question of how to compute the Hessian or its action on a vector.
The implicit function theorem tells us that
\begin{equation}
    dG = -\frac{\partial F}{\partial u}^{-1}\frac{\partial F}{\partial p},
    \label{implicit-function-theorem-dG}
\end{equation}
which will allow us to write down the following expression for the Hessian:
\begin{equation}
    \langle d^2J(p)q, r\rangle = \underbrace{\langle d^2E\cdot dG\cdot q, dG\cdot r\rangle_V}_{\text{1st-order}} + \underbrace{\langle dE, (d^2G\cdot q)r\rangle_V}_{\text{2nd-order}} + \langle d^2R\cdot q, r\rangle_Q
\end{equation}
We can evaluate the first-order term easily enough by using \eqref{implicit-function-theorem-dG}.
It is positive-definite assuming that the model-data misfit functional $E$ is convex, which is the case for, say, $\ell^2$- or $\ell^1$-norm misfits, Huber, etc.
However, the term of second order in the model physics is substantially more difficult to evaluate, to the point of being infeasible for many problems.

The \textbf{Gauss-Newton approximation} is to take just the first-order and regularization terms in the Hessian and ignore the second order terms:
\begin{equation}
    H(p)\cdot q = (dG^*\cdot d^2E\cdot dG + d^2R)\cdot q
    \label{gauss-newton-hessian}
\end{equation}
The Gauss-Newton operator is a positive-definite map from $Q$ to $Q^*$, so its inverse gets us back from the dual to the primal space.
Again, we can expand this further by using equation \eqref{implicit-function-theorem-dG}.
So, we would like to develop a feasible procedure for solving the linear system
\begin{equation}
    H(p)\cdot q = -dJ(p)
    \label{gauss-newton-hessian-inverse}
\end{equation}
where $H$ is the Gauss-Newton approximation to $d^2J$.
The solution of this linear system will be used as the search direction in a Newton-like procedure to update $p$.

In the definition \eqref{gauss-newton-hessian} above, we've written everything in terms of the derivative $dG$ of the model physics with respect to the parameters.
We know by equation \eqref{implicit-function-theorem-dG} that $dG$ can be evaluated by solving a linear PDE.
Rather than have a bunch of operator inverses floating around, we'll rewrite equation \eqref{gauss-newton-hessian-inverse} into something more explicit by introducing two auxiliary fields $v$, $w$, which live in $V$.
We then find that solving the system \eqref{gauss-newton-hessian-inverse} is equivalent to solving the block system
\begin{equation}
    \left[\begin{matrix}d^2R & 0 & \partial_pF^* \\ 0 & d^2E & \partial_uF^* \\ \partial_pF & \partial_uF & 0\end{matrix}\right]\left[\begin{matrix} q \\ v \\ w\end{matrix}\right] = \left[\begin{matrix}-dJ(p) \\ 0 \\ 0\end{matrix}\right]
\end{equation}
for $q$, $v$ and $w$.
Moreover, we can view this block system as equivalent to minimization of the quadratic functional
\begin{equation}
    K(v, q) = \frac{1}{2}\langle d^2R\cdot q, q\rangle + \frac{1}{2}\langle d^2E\cdot v, v\rangle + \langle dJ(p), q\rangle
\end{equation}
subject to the constraint that
\begin{equation}
    \frac{\partial F}{\partial u}v + \frac{\partial F}{\partial p}q = 0.
\end{equation}
Viewed in this light, the auxiliary field $w$ is the Lagrange multiplier that enforces the constraint.

An important point to think about here is that the operator $d^2E$ might not be of full rank; while symmetric and positive semi-definite, some of its eigenvalues might be zero.
To see why this should be so, remember that for your average inverse problem we only get finitely many measurements.
A typical case is where we get several point measurements of the field $u$, in which case
\begin{equation}
    E(u) = \frac{1}{2}\sum_k\left(\frac{|u(x_k) - u^o_k|}{\sigma_k}\right)^2
\end{equation}
where $\{x_k\}$ are the observation points, $u^o_k$ is the observation at point $k$, and $\sigma_k$ the measurement error.
But we can use a finite element basis of arbitrary dimension or a mesh of arbitrarily fine resolution to represent the solution $u$ and the parameters $p$.
In that case, the matrix $d^2E$ will always have zero eigenvalues corresponding to the modes that the observations cannot constrain.
This might seem a little bleak -- how can the Gauss-Newton operator have a well-defined inverse if one of the diagonal blocks isn't really actually honestly positive-definite?
The reason is that the full block $\left[\begin{matrix}d^2R & 0 \\ 0 & d^2E\end{matrix}\right]$ \emph{is} positive-definite \emph{on the kernel of the constraints}, i.e. on the set of all pairs $v$, $q$ such that $\partial_uF\cdot v + \partial_p F\cdot q = 0$!

Now we'd like to use some familiar solution strategy, i.e. projected conjugate gradient or the Uzawa algorithm, to solve the resulting block system.
It's tempting to compute a block $LDL^*$-factorization by hand at this point but as we'll see this doesn't quite work.
Defining the unit lower-triangular matrix
\begin{equation}
    L = \left[\begin{matrix}I & 0 & 0 \\ 0 & I & 0 \\ \partial_pF\cdot d^2R^{-1} & \partial_uF\cdot d^2E^{-1} & I\end{matrix}\right],
\end{equation}
we find that
\begin{equation}
    H = L\left[\begin{matrix}d^2R & & \\ & d^2E & \\ & & -S\end{matrix}\right]L^*
\end{equation}
where
\begin{equation}
    S = \partial_pF\cdot d^2R^{-1}\cdot\partial_pF^* + \partial_uF\cdot d^2E^{-1}\cdot\partial_uF^*.
\end{equation}
is the Schur complement matrix.
The existence of this factorization is contingent upon both $d^2R$ and $d^2E$ having inverses.
A typical choice of the regularization functional $R$ is some penalty for sharp oscillations, i.e.
\begin{equation}
    R(p) = \frac{1}{2}\int_\Omega|\nabla p|^2\ud x,
\end{equation}
in which case $d^2R$ is the Laplace operator.
The existence of an inverse is then subject to certain assumptions about boundary conditions.
For example, if we use only natural boundary conditions, then multiplying the inverse of the Laplace operator is equivalent to solving a Neumann problem, for which the right-hand side must integrate to 0.
Worse yet is the case for $d^2E$; if there are too few measurements, $d^2E$ will not be of full rank, as we described above.

\textbf{So what the hell do we do here?}
A true block factorization is not possible in general.
Can we instead approximate the inverses of these matrices, giving an approximate block factorization?
For example, $d^2E$ might not be invertible because there are either too few or too many measurements for the mesh and finite element basis we've chosen.
In that case, could we instead use a truncated singular value decomposition?
The resulting approximate factorization could then be used as the preconditioner in an iterative method such as MINRES that works for symmetric indefinite problems.


\section{Example}

We'll demonstrate the effectiveness of this approximation for the specific problem of estimating the conductivity parameter in the Poisson equation.
The weak form of the Poisson equation is to find the field $u$ in $H_0^1(\Omega)$ such that, for all test functions $v$ in $H_0^1$,
\begin{equation}
    \int_\Omega k\nabla u\cdot \nabla v\ud x = \int_\Omega fv\ud x.
\end{equation}
Rather than solve directly for $k$, we'll instead parameterize it as
\begin{equation}
    k = k_0e^p
\end{equation}
for some dimensionless field $p$.
The reason for this parameterization is to guarantee that the conductivity is positive for any value of the parameters $p$.
We could do without this parameterization by instead imposing bounds constraints on $k$, but that involves some trickery that we'd rather not have to deal with.
Working with the log-conductivity is very common in the groundwater hydrology literature.

\end{document}
