from scipy import optimize
import firedrake
from firedrake import inner, grad, dx, exp, \
    action, adjoint, replace, assemble, derivative


def _solve(*args, **kwargs):
    """Make all the linear solves use a direct method"""
    firedrake.solve(*args, **kwargs,
                    solver_parameters={'ksp_type': 'preonly', 'pc_type': 'lu'})


class GaussNewton(object):
    def __init__(self, θ, u, F, E, R, dirichlet_ids='on_boundary'):
        self._parameter_space = θ.function_space()
        self._state_space = u.function_space()

        self._parameter = θ
        self._state = u

        self._weak_form = F
        self._objective = E
        self._regularization = R

        self._q = firedrake.Function(self._parameter_space)
        self._f = firedrake.Function(self._parameter_space)

        self._v1 = firedrake.Function(self._state_space)
        self._v2 = firedrake.Function(self._state_space)

        self._bc = firedrake.DirichletBC(self._state_space, 0, dirichlet_ids)

    def mult(self, mat, q, f):
        with self._q.dat.vec as q_vec:
            q.copy(q_vec)

        F, u, θ = self._weak_form, self._state, self._parameter
        dF_dθ = derivative(F, θ)
        dF_du = derivative(F, u)
        dE = derivative(self._objective, u)
        dR = derivative(self._regularization, θ)

        v1, v2 = self._v1, self._v2
        f1 = action(dF_dθ, self._q)
        _solve(dF_du == f1, v1, self._bc)
        f2 = derivative(dE, u, v1)
        _solve(adjoint(dF_du) == f2, v2, self._bc)
        _f = action(adjoint(dF_dθ), v2) + derivative(dR, θ, self._q)
        self._f.assign(assemble(_f))

        with self._f.dat.vec_ro as f_vec:
            f_vec.copy(f)


class InverseProblem(object):
    def _setup(self, θ_initial, f, u_obs, σ, L):
        self._parameter = θ_initial.copy(deepcopy=True)
        self._state = u_obs.copy(deepcopy=True)

        u, θ = self.state, self.parameter
        W = (0.5 * exp(θ) * inner(grad(u), grad(u)) - f * u) * dx
        self._weak_form = derivative(W, u)

        V = u.function_space()
        self._adjoint_state = firedrake.Function(V)
        self._bc = firedrake.DirichletBC(V, 0, 'on_boundary')

        Q = θ.function_space()
        self._search_direction = firedrake.Function(Q)

        self._objective = 0.5 * ((u - u_obs) / σ)**2 * dx
        r = firedrake.Constant(L)**2
        self._regularization = 0.5 * r * inner(grad(θ), grad(θ)) * dx

        dE = derivative(self.objective, u)
        F, λ = self.weak_form, self.adjoint_state
        dF_dθ = derivative(F, θ)
        dE_dθ = action(adjoint(dF_dθ), λ)

        dR = derivative(self.regularization, θ)
        self._derivative = dE_dθ + dR

    def __init__(self, θ_initial, f, u_obs, σ, L):
        """Set up an inverse conductivity problem for the Poisson equation

        Parameters
        ----------
        θ_initial : firedrake.Function
            The initial guess for the parameters we're inferring
        f : firedrake.Function
            The forcing term of the PDE
        u_obs : firedrake.Function
            The observed state that we're fitting to
        σ : firedrake.Function
            The standard deviation of the measurement errors
        L : float
            The regularization parameter; units of [length] / [a]
        """
        self._setup(θ_initial, f, u_obs, σ, L)
        self._update()

    def step(self):
        θ, q = self.parameter, self.search_direction
        t = self._line_search()
        θ += t * q
        self._update()

    def _line_search(self):
        u, θ, q = self.state, self.parameter, self.search_direction
        J = self.objective + self.regularization

        s = firedrake.Constant(0)
        θ_s = θ + s * q
        u_s = u.copy(deepcopy=True)

        F = self.weak_form
        def f(t):
            s.assign(t)
            _solve(replace(F, {θ: θ_s, u: u_s}) == 0, u_s, self._bc)
            return assemble(replace(J, {θ: θ_s, u: u_s}))

        t1, t2, t3, f1, f2, f3, n = optimize.bracket(f)
        result = optimize.minimize_scalar(f, bracket=(t1, t2, t3))
        if not result.success:
            raise ValueError("Line search failed: {}".format(result.message))

        return result.x

    def _update(self):
        u, λ = self.state, self.adjoint_state
        F, E = self.weak_form, self.objective

        _solve(F == 0, u, self._bc)

        dF_du = derivative(F, u)
        dE = derivative(E, u)
        _solve(adjoint(dF_du) == -dE, λ, self._bc)

        self.compute_search_direction()

    def compute_search_direction(self):
        """Compute the current value of the search direction from the
        derivative of the objective functional
        
        The default implementation multiplies by the inverse of the mass
        matrix. Override this method to make something smarter.
        """
        q, dJ = self.search_direction, self.derivative
        Q = q.function_space()
        M = firedrake.TrialFunction(Q) * firedrake.TestFunction(Q) * dx
        _solve(M == -dJ, q)
        q /= firedrake.norm(q)

    @property
    def state(self):
        """The solution of the Poisson equation at the current value of the
        parameters"""
        return self._state

    @property
    def parameter(self):
        """The current guess for the parameters we're inferring"""
        return self._parameter

    @property
    def adjoint_state(self):
        """The solution to the adjoint PDE at the current values of the
        state and parameter fields"""
        return self._adjoint_state

    @property
    def search_direction(self):
        """The direction along which we'll search for a new value of the
        parameters"""
        return self._search_direction

    @property
    def objective(self):
        """The functional measuring the misfit between the current state
        and the observed state"""
        return self._objective

    @property
    def regularization(self):
        """The functional that penalizes sharp gradients in the inferred
        parameter"""
        return self._regularization

    @property
    def weak_form(self):
        """The weak form of the forward model physics"""
        return self._weak_form

    @property
    def derivative(self):
        """The derivative of the whole objective functional with respect to
        the parameter"""
        return self._derivative

